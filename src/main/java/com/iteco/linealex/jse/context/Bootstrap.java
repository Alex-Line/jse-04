package com.iteco.linealex.jse.context;

import com.iteco.linealex.jse.repository.ProjectRepository;
import com.iteco.linealex.jse.repository.TaskRepository;
import com.iteco.linealex.jse.service.ProjectService;
import com.iteco.linealex.jse.service.TaskService;
import com.iteco.linealex.jse.service.ViewService;
import com.iteco.linealex.jse.view.Viewer;

import java.util.Scanner;

public class Bootstrap {

    private static final Scanner scanner = new Scanner(System.in);

    private ProjectService projectService;

    private TaskService taskService;

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    private ViewService viewService;

    private Viewer viewer;

    public Bootstrap() {
        this.projectRepository = new ProjectRepository();
        this.taskRepository = new TaskRepository();
        this.taskService = new TaskService(taskRepository);
        this.projectService = new ProjectService(projectRepository);
        this.viewService = new ViewService(projectService, taskService);
        this.viewer = new Viewer(scanner, viewService);
    }

    public void init() {
        viewer.listenConsole();
    }

}