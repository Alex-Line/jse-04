package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.enumerate.TerminalCommand;
import com.iteco.linealex.jse.util.InsetExistingEntityException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ViewService {

    private ProjectService projectService;

    private TaskService taskService;

    private List<String> help = new ArrayList<>();

    public ViewService(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    public TerminalCommand getCommandByConsoleExpression(String name) {
        for (TerminalCommand terminalCommand : TerminalCommand.values()) {
            if (terminalCommand.getCommand().equals(name)) {
                return terminalCommand;
            }
        }
        return TerminalCommand.DEFAULT;
    }

    public void showHelp() {
        if (!help.isEmpty()) {
            help.forEach(System.out::println);
            System.out.println("");
        } else {
            try (BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/help.txt"))) {
                while (reader.ready()) {
                    help.add(reader.readLine());
                }
                showHelp();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Task selectTask(String taskName) {
        Task task = null;
        if (projectService.getSelectedProject() != null) {
            task = taskService.selectTask(projectService.getSelectedProject().getId(), taskName);
        } else task = taskService.selectTask(taskName);
        return task;
    }

    public boolean attachTask(String projectName) {
        Project project = projectService.getProjectByName(projectName);
        if (taskService.getSelectedTask() == null) {
            return false;
        }
        return taskService.attachTaskToProject(project.getId());
    }

    public Task createTask(String taskName) {
        Task task = null;
        if (projectService.getSelectedProject() != null) {
            task = taskService.createTask(projectService.getSelectedProject().getId(), taskName);
        } else task = taskService.createTask(taskName);
        return task;
    }

    public Collection<Task> showTasks() {
        Collection<Task> collection = null;
        if (projectService.getSelectedProject() != null) {
            collection = taskService.getTasks(projectService.getSelectedProject().getId());
        } else collection = taskService.getTasks();
        return collection;
    }

    public Task removeTask(String taskName) {
        Task task = null;
        if (projectService.getSelectedProject() != null) {
            task = taskService.removeTask(projectService.getSelectedProject().getId(), taskName);
        } else task = taskService.removeTask(taskName);
        return task;
    }

    public Collection<Task> removeAllTasksFromProject() {
        Collection<Task> collection = null;
        if (projectService.getSelectedProject() != null) {
            collection = taskService.removeAllTasksFromProject(projectService.getSelectedProject().getId());
        }
        return collection;
    }

    public Collection<Task> removeAllTasks() {
        return taskService.removeAllTasks();
    }

    public void clearSelection() {
        projectService.clearSelection();
        taskService.clearSelection();
    }

    public Project createProject(String projectName) throws InsetExistingEntityException {
        return projectService.createProject(projectName);
    }

    public Collection<Project> showAllProject() {
        return projectService.getAllProject();
    }

    public Project removeProject(String projectName) {
        Project project = projectService.removeProject(projectName);
        if (project != null) {
            taskService.removeAllTasksFromProject(project.getId());
        }
        return project;
    }

    public void removeAllProjects() {
        Collection<Project> collection = projectService.removeAllProjects();
        for (Project project : collection) {
            taskService.removeAllTasksFromProject(project.getId());
        }
    }

    public void showAllTaskFromProject(String projectName) {
        String projectId = projectService.getProjectByName(projectName).getId();
        taskService.getTasks(projectId);
    }

    public Project selectProject(String projectName) {
        return projectService.selectProject(projectName);
    }

}
