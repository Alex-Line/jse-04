package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.repository.TaskRepository;

import java.util.Collection;

public class TaskService {

    private TaskRepository taskRepository;

    private Task selectedTask = null;


    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task selectTask(String projectId, String taskName) {
        if (projectId == null || projectId.isEmpty()) selectedTask = selectTask(taskName);
        if (taskName == null || taskName.isEmpty()) return null;
        selectedTask = taskRepository.findOne(taskName, projectId);
        return selectedTask;
    }

    public Task selectTask(String taskName) {
        if (taskName == null || taskName.isEmpty()) return null;
        selectedTask = taskRepository.findOne(taskName);
        return selectedTask;
    }

    public Task createTask(String taskName) {
        if (taskName == null || taskName.isEmpty()) return null;
        Task task = null;
        if (taskRepository.contains(taskName)) {
            task = new Task();
            task.setName(taskName);
            selectedTask = task;
            taskRepository.persist(task);
        }
        return task;
    }

    public Task createTask(String projectId, String taskName) {
        if (projectId == null || projectId.isEmpty()) return createTask(taskName);
        if (taskName == null || taskName.isEmpty()) return null;
        Task task = null;
        if (taskRepository.contains(projectId, taskName)) {
            task = new Task();
            task.setName(taskName);
            task.setProjectId(projectId);
            selectedTask = task;
            taskRepository.persist(task, projectId);
        }
        return task;
    }

    public Collection<Task> getTasks() {
        Collection<Task> collection = taskRepository.findAll();
        for (Task task : collection) {
            if (task.getProjectId() != null) {
                collection.remove(task);
            }
        }
        return collection;
    }

    public Collection<Task> getTasks(String projectId) {
        if (projectId == null || projectId.isEmpty()) return getTasks();
        Collection<Task> collection = taskRepository.findAll();
        for (Task task : collection) {
            if (task.getProjectId() == null) {
                collection.remove(task);
                continue;
            }
            if (!task.getProjectId().equals(projectId)) {
                collection.remove(task);
                continue;
            }
        }
        return collection;
    }

    public Task removeTask(String taskName) {
        if (taskName == null || taskName.isEmpty()) return null;
        return taskRepository.remove(taskName);
    }

    public Task removeTask(String projectId, String taskName) {
        if (projectId == null || projectId.isEmpty()) return removeTask(taskName);
        if (taskName == null || taskName.isEmpty()) return null;
        return taskRepository.remove(taskName, projectId);
    }

    public Collection<Task> removeAllTasksFromProject(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        Collection<Task> collection = taskRepository.findAll(projectId);
        for (Task task : collection) {
            taskRepository.remove(task.getName());
        }
        return collection;
    }

    public Collection<Task> removeAllTasks() {
        return taskRepository.removeAll();
    }

    public void clearSelection() {
        selectedTask = null;
    }

    public boolean attachTaskToProject(String projectId) {
        if (projectId == null || projectId.isEmpty()) return false;
        for (Task task : taskRepository.findAll()) {
            if (task.getProjectId() != null) continue;
            if (!task.getId().equals(selectedTask)) continue;
            task.setProjectId(projectId);
            return true;
        }
        return false;
    }

    public Task getSelectedTask() {
        return selectedTask;
    }

}