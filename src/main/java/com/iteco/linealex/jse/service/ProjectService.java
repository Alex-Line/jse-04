package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.repository.ProjectRepository;
import com.iteco.linealex.jse.util.InsetExistingEntityException;

import java.util.Collection;

public class ProjectService {

    private Project selectedProject = null;

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project selectProject(String projectName) {
        selectedProject = null;
        if (projectName == null || projectName.isEmpty()) return selectedProject;
        selectedProject = getProjectByName(projectName);
        return selectedProject;
    }

    public Project createProject(String projectName) throws InsetExistingEntityException {
        Project project = null;
        if (projectName == null || projectName.isEmpty()) return project;
        selectedProject = null;
        if (projectRepository.findOne(projectName) == null) {
            project = new Project();
            project.setName(projectName);
            projectRepository.persist(project);
            selectedProject = project;
        }
        return project;
    }

    public Project persist(Project project) throws InsetExistingEntityException {
        if (project == null) return null;
        return projectRepository.persist(project);
    }

    public Collection<Project> getAllProject() {
        return projectRepository.findAll();
    }

    public Project removeProject(String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        Project project = projectRepository.remove(projectName);
        if (project != null) selectedProject = null;
        return project;
    }

    public Collection<Project> removeAllProjects() {
        return projectRepository.removeAll();
    }

    public void clearSelection() {
        selectedProject = null;
    }

    public Project getProjectByName(String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        return projectRepository.findOne(projectName);
    }

    public Project getSelectedProject() {
        return selectedProject;
    }

}