package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.util.DateFormatter;

import java.util.Date;
import java.util.UUID;

public class Project implements IEntity {

    private String id = UUID.randomUUID().toString();

    private String name = "unnamed project";

    private String description = "";

    private Date dateStart = new Date();

    private Date dateFinish = new Date();

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public String toString() {
        return "Project: " + name +
                " { ID = " + id +
                ",\n    description = '" + description + '\'' +
                ",\n    Start Date = " + DateFormatter.formatDate(dateStart) +
                ",\n    Finish Date = " + DateFormatter.formatDate(dateFinish) +
                " }";
    }

}
