package com.iteco.linealex.jse.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

    private static SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    public static String formatDate(Date date) {
        return format.format(date);
    }

}
