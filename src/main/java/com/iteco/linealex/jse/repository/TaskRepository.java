package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.util.InsetExistingEntityException;

import java.util.*;

public class TaskRepository implements IRepository<Task> {

    private Map<String, Task> tasks = new LinkedHashMap<>();

    @Override
    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Collection<Task> findAll(String projectId) {
        Map<String, Task> map = new LinkedHashMap<>();
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (entry.getValue().getProjectId() == null) continue;
            if (entry.getValue().getProjectId() != null && entry.getValue().getProjectId().equals(projectId)) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
        return map.values();
    }

    @Override
    public Task findOne(String taskName) {
        Task task = null;
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() != null) continue;
            task = entry.getValue();
        }
        return task;
    }

    public Task findOne(String taskName, String projectId) {
        Task task = null;
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() == null) continue;
            if (entry.getValue().getProjectId().equals(projectId)) continue;
            task = entry.getValue();
        }
        return task;
    }

    @Override
    public Task persist(Task example) {
        Task task = null;
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (!entry.getValue().getName().equals(example.getName())) continue;
            if (entry.getValue().getProjectId() != null) continue;
            task = example;
            tasks.put(example.getId(), example);
        }
        return task;
    }

    public Task persist(Task example, String projectId) {
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            Task task = entry.getValue();
            if (!task.getName().equals(example.getName())) return null;
            if (task.getProjectId() == null) return null;
            if (!task.getProjectId().equals(projectId)) return null;
            tasks.put(example.getId(), example);
            return task;
        }
        return null;
    }

    @Override
    public Task merge(Task example) {
        Task oldTask = findOne(example.getName());
        if (oldTask == null) return oldTask;
        oldTask.setName(example.getName());
        oldTask.setDescription(example.getDescription());
        oldTask.setDateStart(example.getDateStart());
        oldTask.setDateFinish(example.getDateFinish());
        oldTask.setProjectId(example.getProjectId());
        persist(example);
        return oldTask;
    }

    @Override
    public Task remove(String taskName) {
        Task task = null;
        for (Iterator<Map.Entry<String, Task>> iterator = tasks.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, Task> entry = iterator.next();
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!(entry.getValue().getProjectId() == null)) continue;
            task = entry.getValue();
            tasks.remove(entry.getKey());
        }
        return task;
    }

    public Task remove(String taskName, String projectId) {
        for (Iterator<Map.Entry<String, Task>> iterator = tasks.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, Task> entry = iterator.next();
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() != null) continue;
            if (!entry.getValue().getProjectId().equals(projectId)) continue;
            return tasks.remove(entry.getKey());
        }
        return null;
    }

    @Override
    public Collection<Task> removeAll() {
        Map<String, Task> initialMap = new LinkedHashMap<>(tasks);
        tasks.clear();
        return initialMap.values();
    }

    public boolean contains(String taskName) {
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (entry.getValue().getProjectId() != null) continue;
            if (entry.getValue().getName().equals(taskName)) {
                return true;
            }
        }
        return false;
    }

    public boolean contains(String projectId, String taskName) {
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            if (!entry.getValue().getProjectId().equals(projectId)) continue;
            if (entry.getValue().getName().equals(taskName)) {
                return true;
            }
        }
        return false;
    }

}
