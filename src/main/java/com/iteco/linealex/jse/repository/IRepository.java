package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.util.InsetExistingEntityException;

import java.util.Collection;
import java.util.Map;

public interface IRepository<T> {

    public Collection<T> findAll();

    public T findOne(String name);

    public T persist(T example) throws InsetExistingEntityException;

    public T merge(T example);

    public T remove(String name);

    public Collection<T> removeAll();

}
