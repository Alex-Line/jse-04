package com.iteco.linealex.jse.view;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.enumerate.TerminalCommand;
import com.iteco.linealex.jse.service.ViewService;
import com.iteco.linealex.jse.util.InsetExistingEntityException;

import java.util.Collection;
import java.util.Map;
import java.util.Scanner;

public class Viewer {

    private Scanner scanner;

    private ViewService viewService;

    public Viewer(Scanner scanner, ViewService viewService) {
        this.scanner = scanner;
        this.viewService = viewService;
    }

    public void listenConsole() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (true) {
            switch (getCommandByConsoleExpression(scanner.nextLine().trim().toLowerCase())) {
                case PROJECT_CREATE:
                    createProject(askProjectName());
                    break;
                case PROJECT_LIST:
                    showAllProjects();
                    break;
                case PROJECT_REMOVE:
                    removeProject(askProjectName());
                    break;
                case PROJECT_CLEAR:
                    removeAllProjects();
                    break;
                case PROJECT_SELECT:
                    selectProject(askProjectNameForSelection());
                    break;
                case TASK_SELECT:
                    selectTask(askTaskNameForSelection());
                    break;
                case TASK_ATTACH:
                    attachTask(askProjectName());
                    break;
                case TASK_CREATE:
                    createTask(askTaskName());
                    break;
                case TASK_LIST:
                    showTasks();
                    break;
                case TASK_REMOVE:
                    removeTask(askTaskName());
                    break;
                case TASK_CLEAR:
                    removeAllTasksFromProject();
                    break;
                case TASK_CLEAR_ALL:
                    removeAllTasks();
                    break;
                case CLEAR_SELECTION:
                    clearSelection();
                    break;
                case EXIT:
                    executeExit();
                    break;
                case HELP:
                    viewService.showHelp();
                    break;
                default:
                    System.out.println("[UNKNOWN COMMAND! PLEASE TRY AGAIN]\n");
            }
        }
    }

    private TerminalCommand getCommandByConsoleExpression(String command) {
        TerminalCommand terminalCommand = viewService.getCommandByConsoleExpression(command);
        if (terminalCommand == TerminalCommand.DEFAULT) {
            System.out.println("THERE IS NOT SUCH COMMAND! PLEASE TRY ENTER COMMAND AGAIN\n");
            return TerminalCommand.HELP;
        }
        return terminalCommand;
    }

    private String askProjectName() {
        System.out.println("[ENTER PROJECT NAME]");
        return scanner.nextLine().trim();
    }

    private String askProjectNameForSelection() {
        System.out.println("[ENTER THE NAME OF PROJECT FOR SELECTION]");
        return scanner.nextLine().trim();
    }

    private String askTaskName() {
        System.out.println("[ENTER TASK NAME]");
        return scanner.nextLine().trim();
    }

    private String askTaskNameForSelection() {
        System.out.println("[ENTER THE NAME OF TASK FOR SELECTION]");
        return scanner.nextLine().trim();
    }

    private void createProject(String projectName) {
        Project project = null;
        try {
            project = viewService.createProject(projectName);
        } catch (InsetExistingEntityException e) {
            System.out.println("[THERE IS ALREADY THE PROJECT WITH NAME: " + projectName + "]\n");
        }
        if (project != null) {
            System.out.println("[OK]\n");
        } else System.out.println("[THERE IS ALREADY THE PROJECT WITH NAME: " + projectName + "]\n");
    }

    private void showAllProjects() {
        System.out.println("[PROJECT LIST]");
        Collection<Project> collection = viewService.showAllProject();
        if (!collection.isEmpty()) {
            int index = 1;
            for (Project project : viewService.showAllProject()) {
                System.out.println(index + ". " + project);
                index++;
            }
            System.out.println();
        } else System.out.println("[THERE IS NOT ANY PROJECTS YET]\n");
    }

    private void removeProject(String projectName) {
        if (viewService.removeProject(projectName) != null) {
            removeAllTasksFromProject();
        } else System.out.println("[THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!]\n");
    }

    private void removeAllProjects() {
        viewService.removeAllProjects();
        System.out.println("[All PROJECTS REMOVED]\n");
    }

    private void selectProject(String projectName) {
        Project project = viewService.selectProject(projectName);
        if (project != null) {
            System.out.println(project);
            System.out.println("[OK]\n");
        } else System.out.println("[THERE IS NOT SUCH PROJECT AS " + projectName
                + ". PLEASE TRY TO SELECT AGAIN]\n");
    }

    private void selectTask(String taskName) {
        Task task = viewService.selectTask(taskName);
        if (task != null) {
            if (task.getProjectId() != null) {
                System.out.println("[WAS SELECTED IN THE PROJECT WITH ID" + task.getProjectId() + "]");
                System.out.println(task + "\n");
            } else {
                System.out.println("[WAS SELECTED WITHOUT PROJECT]");
                System.out.println(task + "\n");
            }
        } else System.out.println("[THERE IS NOT SUCH TASK AS " + taskName
                + ". PLEASE TRY TO SELECT AGAIN OR SELECT PROJECT FIRST]\n");
    }

    private void attachTask(String projectName) {
        if(viewService.attachTask(projectName)) System.out.println("[OK]\n");
        else System.out.println("THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!\n");
    }

    private void createTask(String taskName) {
        Task task = viewService.createTask(taskName);
        if (task != null) {
            System.out.println("[OK]\n");
        } else System.out.println("[THERE IS SUCH TASK ALREADY]\n");
    }

    private void showTasks() {
        Collection<Task> collection = viewService.showTasks();
        if (!collection.isEmpty()) {
            System.out.println("[TASK LIST]");
            int index = 1;
            for (Task task : collection) {
                System.out.println(index + ". " + task);
                index++;
            }
            System.out.println();
        } else System.out.println("[THERE IS NOT ANYTHING TO LIST]\n");
    }

    private void removeTask(String taskName) {
        System.out.println("REMOVING TASK...");
        Task task = viewService.removeTask(taskName);
        if (task != null) {
            System.out.println("[OK]\n");
        } else System.out.println("[THERE IS NOT SUCH TASK AS " + taskName
                + ". PLEASE TRY AGAIN OR SELECT PROJECT FIRST]\n");
    }

    private void removeAllTasksFromProject() {
        Collection<Task> collection = viewService.removeAllTasksFromProject();
        if (collection != null) {
            System.out.println("[REMOVING TASKS FROM PROJECT]");
            System.out.println("[ALL TASKS REMOVED]");
            System.out.println("[OK]\n");
        } else System.out.println("[YOU DID NOT SELECT ANY PROJECT! SELECT ANY AND TRY AGAIN]\n");
    }

    private void removeAllTasks() {
        if (viewService.removeAllTasks() != null) {
            System.out.println("[ALL TASKS FROM ALL PROJECTS ARE REMOVED]\n");
        }
        System.out.println("[THERE IS NOT ANY TASK]");
    }

    private void clearSelection() {
        viewService.clearSelection();
        System.out.println("[ALL SELECTION WAS CANCELED]\n");
    }

    public void executeExit() {
        System.exit(0);
    }

}
