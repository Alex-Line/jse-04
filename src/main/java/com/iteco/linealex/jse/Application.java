package com.iteco.linealex.jse;

import com.iteco.linealex.jse.context.Bootstrap;

public class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
